# API

Mubody main class is called Mission. This class contains the main
methods to generate orbits in L2 (the second lagrangian point) of the
Earth-Sun system. The Mission class and its methods have default values
that allow to generate orbits directly.

```{eval-rst}
.. autoclass:: mubody.mission.Mission
   :members:
´´´