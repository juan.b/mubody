mubody
======

Author
======

Juan Bermejo Ballesteros (juan.bermejo@protonmail.com)


Description
===========

mubody is an astrodynamics open-source Python library focused on the
libration points and the dynamics of the multi-body regime from which
they emerge. Its aim is to be used as a tool for the design and the
analysis of space missions in the libration points. It has been
developed by students from the Technical University from Madrid (Spain).


Installation
============

Please check the [Installation Guide](https://gitlab.com/juan.b/mubody/-/wikis/Installation%20Guide) in the project [wiki](https://gitlab.com/juan.b/mubody/-/wikis).



Documentation
=============

Basic documentation can be found [here](https://mubody.readthedocs.io/) and some examples can also be found in the project [wiki](https://gitlab.com/juan.b/mubody/-/wikis).


Contributing
=============

mubody is open to contributions and collaborations. If you want to
participate as part of your Bachelor dissertation, Master's thesis, or
just as a hobby, contact us [here](mubody@proton.me)!


Problems
========

If you find any problem using mubody (quite probable to be fair), please
open an issue in the [issue tracker](https://gitlab.com/mubody/mubody/-/issues).


Citation
========

If you have used mubody, please cite us using:

    Bermejo Ballesteros, J. [et al.]. Mubody, an astrodynamics open-source Python library focused on libration points. A: "4th Symposium on Space Educational Activities". Universitat Politècnica de Catalunya, 2022


License
=======

mubody is released under the MIT license, hence allowing commercial
use of the library. Please refer to the
[LICENSE](https://gitlab.com/mubody/mubody/-/blob/master/LICENSE.txt) file

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://gitlab.com/mubody/mubody/-/blob/master/LICENSE.txt)


Contact
=======

For any questions you can contact us using:

    mubody@proton.me

