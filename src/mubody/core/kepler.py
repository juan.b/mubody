import numpy as np
from scipy import optimize
from astropy import units as u


def Bm(rper, Vinf, mu):

    Bmod = rper/Vinf * np.sqrt(Vinf**2 + 2 * mu/rper)

    return Bmod


def DeltaV_fb(rper, Vinf, mu):

    DV = 2 * Vinf/(1 + rper * Vinf**2/mu)

    return DV


def rocket_eq(Isp, mi, mf):
    """
    Tsiolkovsky rocket equation

    Parameters
    ----------
    Isp : float
        Engine specific impulse (s)
    mi : float
        Initial mass (m0 + mp) (kg)
    mf : float
        Final mass after burn (m0) (kg)

    Returns
    -------
    DV : float
        Resulting delta-v (m/s)

    """

    DV = Isp * 9.81 * np.log(mi/mf)

    return DV


def rocket_eq_mi(Isp, DV, mf):
    """
    Tsiolkovsky rocket equation inverse to obtain initial mass

    Parameters
    ----------
    Isp : float
        Engine specific impulse (s)
    DV : float
        Delta-v performed (m/s)
    mf : float
        Final mass after burn (m0) (kg)

    Returns
    -------
    mi : float
        Initial mass (m0 + mp) (kg)

    """

    mi = mf * np.exp(DV/(Isp * 9.8))

    return mi


def reqins(Isp, DV, mf):

    mi = [mf]
    for impulse in DV:
        mi.append(rocket_eq_mi(Isp, impulse, mi[-1]))

    mi.reverse()

    return mi


def lambert(r1, r2, alpha, TOF, nu_guess=0):
    """
    Solves lambert problem for circular and coplanar orbits

    Parameters
    ----------
    r1 : float
        semi-major axis of departure planet
    r2 : float
        semi-major axis of arrival planet
    alpha : float
        Angle between r1 and r2 vectors (rad)
    TOF : float
        Time of flight (days)
    nu_guess : float, optional
        Initial guess for departure TA

    Returns
    -------
    a : float
        semi-major axis of transfer orbit (km)
    e : ndarray
        eccentricity of transfer orbit
    nu : float, optional
        TA of departure

    """

    def lambert_equation(nu0):
        """
        Computes difference between desired TOF and current TOF

        Parameters
        ----------
        nu0 : float
            Departure TA

        Returns
        -------
        residue : float
            Difference between TOFs

        References
        ----------
        .. [1] Elices T. (1991). Introduccion a la Dinanica Espacial. INTA. (Corrected)

        """

        e = (r2 - r1)/(r1 * np.cos(nu0) - r2 * np.cos(nu0 + alpha))

        f = np.sqrt(1 - e**2)

        q1 = 2 * np.arctan(f * np.tan((alpha + nu0)/2)/(1 + e))
        q2 = (e * f * np.sin(alpha + nu0))/(1 + e * np.cos(alpha + nu0))
        q3 = 2 * np.arctan((f * np.tan(nu0/2))/(1 + e))
        q4 = e * f * np.sin(nu0)/(1 + e * np.cos(nu0))

        residue = TOF - 365.25/(2 * np.pi) * (q1 - q2 + q3 - q4)

        return residue

    nu = optimize.fsolve(lambert_equation, x0=nu_guess)[0]

    e = (r2 - r1)/(r1 * np.cos(nu) - r2 * np.cos(nu + alpha))
    p = r1 * (1 + e * np.cos(nu))
    a = p/(1 - e**2)

    return a, e, nu


def energy(mu, a):
    """
    Computes orbit energy

    Parameters
    ----------
    mu : float
        standard gravitational parameter of the central body (km3/s2)
    a : float
        semi-major axis of the orbit (km)

    Returns
    -------
    energy : float
        Orbit energy (km2/s2)
    """

    energy = -mu/(2 * a)

    return energy


def velocity_from_energy(mu, energy, r):
    """
    Computes velocity in a given orbital position from energy

    Parameters
    ----------
    mu : float
        standard gravitational parameter of the central body (km3/s2)
    energy : float
        sorbital energy (km2/s2)
    r : float
        distance to central body (km)

    Returns
    -------
    v : float
        Orbital velocity (km/s)
    """

    v = np.sqrt(2 * (energy + mu/r))

    return v


def velocity_circular_orbit(mu, a):
    """
    Computes velocity of a circular orbit

    Parameters
    ----------
    mu : float
        standard gravitational parameter of the central body (km3/s2)
    a : float
        semi-major axis of the orbit (km)

    Returns
    -------
    v : float
        Orbital velocity (km/s)
    """

    v = np.sqrt(mu/a)

    return v


def solve_kepler_equation(n, e, TOF, E=0.1):
    """
    Solves kepler equation iteratively

    Parameters
    ----------
    n : float
        Mean angular motion (rad/s)
    e : float
        Orbit eccentricity
    E : float
        Eccentric anomaly, staring guess (rad)

    Returns
    -------
    E : float
        Eccentric anomaly, converged value (rad)
    """

    error_threshold = 1e-3
    error = 1

    while error > error_threshold:
        E0 = E
        E = (n * TOF + e * np.sin(E)) * u.rad
        error = abs((E0 - E)/E0)

    return E


def ss_inclination(a, e=0):
    """
    Calculates critical inclination for a sunsynchronous orbit around Earth

    Parameters
    ----------
    a : float
        semi-major axis of the orbit (km)
    e : float
        Orbit eccentricity (optional)

    Returns
    -------
    i : float
        Inclination of the sunsynchronous orbit (deg)
    """

    mu = 3.986e5
    T = 365.25 * 86400
    J2 = 1.0826e-3
    Re = 6378

    omega_sun = 2 * np.pi/T
    n = np.sqrt(mu/a**3)
    p = a * (1 - e**2)

    i = np.arccos(- 2 * omega_sun * p**2/(3 * n * Re**2 * J2)) * 180/np.pi

    return i
