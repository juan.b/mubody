# Propagating an orbit

Mubody also allows to provide the initial conditions (IC) vector of the
satellite (position and velocity) and integrate the equations of motion
directly to obtain the trajectory.

```python
from mubody.mission import Mission
import matplotlib.pyplot as plt
```

First, we generate an analytic orbit so we can compare it with the
trajectory integrated from the IC.

```python
upm_a = Mission(mission_time=365*86400)
upm_a.AO()
```
We can plot the trajectory and save the figure in a variable to reuse
it later.

```python
fig = upm_a.plot_trajectory()

plt.show()
```

```{figure} ../_static/lissajous_AO_short.png
---
alt: Plot of the reference orbit
---
```

Now, we can retrieve the initial conditions from the previous case and
reuse it in a new mission. The trajectory of the satellite, and
therefore its initial conditions, are expressed in a given coodinate
frame. By default, the coordinate frame of the CRTBP is Synodic and
centered in the second Lagrange point of the the Sun-Earth/Moon system.

```python
IC, frame = upm_a.sat.orbit.IC()
print(frame)
print(IC)
```
```python
SEML2Synodic
[[-1.20000000e+08]
 [-1.68941915e-06]
 [-3.31550837e-06]
 [-2.21489493e-13]
 [ 1.57392553e+02]
 [-1.19105000e+02]]
```

With the IC and its corresponding frame, we can integrate the motion for
the desired time and plot it along with the analytic orbit.

```python
upm_b = Mission(mission_time=180*86400)
upm_b.IC(IC=IC, IC_frame=frame)
fig = upm_b.plot_trajectory(fig=fig)

plt.show()
```


```{figure} ../_static/lissajous_AO.png
---
alt: Plot of the reference orbit and the propagated trajectory
---
```


As it can be seen in the figure, the unstability of the motion around
the libration point, which is not present in the analytic solution,
drives the satellite away from the region when the equations of motion
are integrated directly, even if the IC are the same.


