# Bibliography


## Basic

* D. Vallado, W. McClain, J.Wertz, Fundamentals of astrodynamics and
applications, 4th Edition, Microcosm Press, 2013.


## Specific

### Lissajous orbit generation, targeting method

* HOWELL, K. C.; PERNICKA, H. J. Numerical determination of Lissajous
trajectories in the restricted three-body problem. Celestial Mechanics,
1987, vol. 41, no 1-4, p. 107-124.

### Halo orbit generation

* D. L. Richardson, Analytic construction of periodic orbits about the
collinear points, Celestial Mechanics 22 (3) (1980) 241–253.
doi:10.1007/BF01229511

* Howell, K. C. Three-dimensional, periodic, ’halo’ orbits. Celestial
Mechanics, 32:53–71, 1984.