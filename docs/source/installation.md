# Installation Guide

Brief guide to install mubody package in anaconda.

1.  Clone repository

    Clone mubody repository in your computer, in a directory of your preference

2.  Install Anaconda

    Any recent release is valid

        https://www.anaconda.com/distribution/

3.  Make conda install the newest version of a package in any listed channel

    Open Anaconda terminal and run

        $conda config --set channel_priority false

4.  Add conda-forge channel

    In Anaconda terminal run

        $conda config --add channels conda-forge

5.  In Anaconda terminal, open the repository folder and create an specfic environment for mubody

    This will create a new environment called 'mubody-env' and install all the required packages for mubody to work

        $conda env create -f condenv.yml

6.  Activate the environment you just created

        $conda activate mubody-env

7.  Install mubody lib

    In Anaconda terminal, with mubody-env activated, open the repository folder and run

        $pip install --editable .
