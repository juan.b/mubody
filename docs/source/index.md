# Welcome to mubody's documentation!

## About

mubody is an open source Python library focused in multi-body dynamics. It's aim is to be used as a tool for the analysis of space missions in the Lagrangian points. It has been developed by students from UPM.


## Contents

```{toctree}
---
maxdepth: 2
caption: Getting started
---
installation
examples
```

```{toctree}
---
maxdepth: 2
caption: Reference
---
api
bib
```